const path = require('path');
module.exports = {
  root: true,
  settings: {
    "import/resolver": {
      node: {
        extensions: [
          ".js",
          ".ts"
        ]
      }
    }
  },
  parserOptions: {
    parser: "@typescript-eslint/parser",
    project: path.resolve(__dirname, './tsconfig.json'),
    tsconfigRootDir: __dirname,
    ecmaVersion: 2018,
    sourceType: "module"
  },
  extends: [
    "airbnb-typescript/base",
    "plugin:@typescript-eslint/recommended",
  ],
  plugins: [
    "@typescript-eslint",
  ],
  rules: {
    "import/no-cycle": "off",
    "no-param-reassign": "off",
    "@typescript-eslint/no-unused-vars": "off",
    "@typescript-eslint/explicit-member-accessibility": "off",
  }
};
