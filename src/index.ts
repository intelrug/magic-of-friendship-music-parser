import { createConnection } from 'typeorm';
import Music from './models/Music';
import Artist from './models/Artist';
import Album from './models/Album';
import Parser from './parser';

createConnection({
  type: 'mysql',
  host: process.env.DB_HOST,
  port: parseInt(process.env.DB_PORT, 10),
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  synchronize: true,
  logging: true,
  entities: [
    Music,
    Artist,
    Album,
  ],
}).then((): void => {
  Parser.parse();
});
