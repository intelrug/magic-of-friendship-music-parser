import {
  Column,
  Entity, JoinColumn, ManyToOne, ObjectType, OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import Music from './Music';
import Artist from './Artist';

@Entity('albums')
export default class Album {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    nullable: false,
    type: 'int',
  })
  artist_id: number;

  @Column({
    nullable: false,
    type: 'varchar',
  })
  name: string;

  @ManyToOne((): ObjectType<Artist> => Artist, (artist): Album[] => artist.albums)
  @JoinColumn({ name: 'artist_id' })
  artist: Artist;

  @OneToMany((): ObjectType<Music> => Music, (music): Album => music.album)
  songs: Music[];
}
