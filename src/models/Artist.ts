import {
  Column,
  Entity, ObjectType, OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import Music from './Music';
import Album from './Album';

@Entity('artists')
export default class Artist {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    nullable: false,
    type: 'varchar',
  })
  name: string;

  @OneToMany((): ObjectType<Music> => Music, (music): Artist => music.artist)
  songs: Music[];

  @OneToMany((): ObjectType<Album> => Album, (album): Artist => album.artist)
  albums: Album[];
}
