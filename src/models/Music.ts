import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  ObjectType,
  PrimaryGeneratedColumn,
} from 'typeorm';
import Artist from './Artist';
import Album from './Album';

@Entity('music')
export default class Music {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    nullable: false,
    type: 'varchar',
  })
  link: string;

  @Column({
    nullable: false,
    type: 'int',
  })
  artist_id: number;

  @Column({
    nullable: true,
    type: 'int',
  })
  album_id: number;

  @Column({
    nullable: false,
    type: 'varchar',
  })
  title: string;

  @Column({
    nullable: false,
    type: 'int',
    default: 0,
  })
  duration: number;

  @Column({
    nullable: false,
    type: 'int',
  })
  size: number;

  @ManyToOne((): ObjectType<Artist> => Artist, (musician): Music[] => musician.songs)
  @JoinColumn({ name: 'artist_id' })
  artist: Artist;

  @ManyToOne((): ObjectType<Album> => Album, (album): Music[] => album.songs)
  @JoinColumn({ name: 'album_id' })
  album: Album;
}
