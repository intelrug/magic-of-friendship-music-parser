/* eslint-disable import/no-duplicates,no-await-in-loop */
import * as fs from 'fs';
import * as mm from 'music-metadata';
import * as path from 'path';
import { IAudioMetadata } from 'music-metadata';
import { Stats } from 'fs';
import { getConnection, getRepository, Repository } from 'typeorm';
import Music from './models/Music';
import Artist from './models/Artist';
import Album from './models/Album';

export default class Parser {
  private static artists: Artist[] = [];

  private static albums: Album[] = [];

  public static async parse(): Promise<void> {
    const music: Music[] = [];
    const directory = process.env.SOURCE_PATH;
    const files: string[] = Parser.getFiles(directory);
    for (let i = 0; i < files.length; i += 1) {
      const file = files[i];
      try {
        const metadata: IAudioMetadata = await mm.parseFile(file);
        const rep: Repository<Music> = getRepository(Music);
        const pathNormalized: string = file.slice(path.normalize(directory).length).replace(/\\/g, '/');
        const pathArray: string[] = pathNormalized.split('/');
        const artist: Artist = await Parser.getArtist(pathArray[0]);
        const album: Album = await Parser.getAlbum(artist.id, metadata.common.album);
        let { title } = metadata.common;
        if (!title) title = pathArray[pathArray.length - 1].slice(0, -5);
        const song: Music = rep.create({
          artist_id: artist.id,
          album_id: album ? album.id : null,
          title,
          duration: metadata.format.duration / 1.0884,
          size: Parser.getFilesizeInBytes(file),
          link: pathNormalized,
        });
        music.push(song);
      } catch (e) {
        console.error(e);
      }
    }
    await getConnection().transaction(async (transactionalEntityManager): Promise<void> => {
      for (let i = 0; i < music.length; i += 1) {
        await transactionalEntityManager.save(music[i]);
      }
    });
  }

  private static getFiles(directory: string): string[] {
    let filesAll: string[] = [];
    const files: string[] = fs.readdirSync(directory);
    files.forEach((folder): void => {
      const p: string = path.join(directory, folder);
      const stats: Stats = fs.statSync(p);
      if (stats.isDirectory()) {
        filesAll = [...filesAll, ...Parser.getFiles(p)];
        return;
      }
      filesAll.push(p);
    });

    return filesAll;
  }

  private static getFilesizeInBytes(filename): number {
    const stats = fs.statSync(filename);
    return stats.size;
  }

  private static async getArtist(name): Promise<Artist> {
    let artist = this.artists.find((a): boolean => a.name === name);
    let rep: Repository<Artist>;
    if (!artist) {
      rep = getConnection().getRepository(Artist);
      artist = await rep.findOne({ where: { name } });
    }
    if (!artist) {
      artist = rep.create({ name });
      artist = await rep.save(artist);
    }
    this.artists.push(artist);

    return artist;
  }

  private static async getAlbum(artistId, name): Promise<Album> {
    if (!name) return null;
    let album = this.albums.find((a): boolean => a.name === name && a.artist_id === artistId);
    let rep: Repository<Album>;
    if (!album) {
      rep = getConnection().getRepository(Album);
      album = await rep.findOne({ where: { artist_id: artistId, name } });
    }
    if (!album) {
      album = rep.create({ name, artist_id: artistId });
      album = await rep.save(album);
    }
    this.albums.push(album);

    return album;
  }
}
